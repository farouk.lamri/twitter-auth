package service;

import bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import repository.UserRepository;

public interface authService {

    User findByUsername(String username);
}
